<?php
use Slim\Http\Request;
use Slim\Http\Response;
use \Firebase\JWT\JWT;



// get all airports
$app->get('/airports', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM airports ");
    $sth->execute();
    $airports = $sth->fetchAll();
    return $this->response->withJson($airports);
});

// get all airport names
$app->get('/airport_name', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT name FROM airports ");
    $sth->execute();
    $airports = $sth->fetchAll();
    return $this->response->withJson($airports);
});

// get airports ahead
$app->get('/airports_ahead/{origin}', function ($request, $response, $args) {
    
    $airport_name = '%'.$args['origin'].'%';
    $query = "SELECT name, city, country, iata_code, type FROM airports 
              WHERE (name like ? OR city like ? OR iata_code like ?) 
              AND (type = 'large_airport' OR type = 'medium_airport') AND iata_code <> ''
              ORDER BY FIELD (type,'large_airport', 'medium_airport')";
  
    $sth = $this->db->prepare($query);
    $sth->bindParam(1, $airport_name);
    $sth->bindParam(2, $airport_name);
    $sth->bindParam(3, $airport_name);
    $sth->execute();
    $rows = array();
    while($r = $sth->fetch(PDO::FETCH_ASSOC)) {
         $airports[] = $r["iata_code"]." - ".$r['name'].' - '.$r['city']." - ".$r['country'];
    }
    return $this->response->withJson($airports);
});

// get fliths with two iata_code
$app->get('/flights[/{origin}[/{destination}]]', function ($request, $response, $args) {
      $query = "SELECT fli.*, air.name as ori_name, air.city as ori_city, air2.name as dest_name, air2.city as dest_city,
                       line.name as airline_name
                FROM flights fli
                INNER JOIN airports air ON fli.originAirport = air.iata_code
                INNER JOIN airports air2 ON fli.destinationAirport = air2.iata_code
                INNER JOIN airlines line ON fli.airline = line.code 
                WHERE fli.originAirport = ? AND fli.destinationAirport = ?";
  
    $sth = $this->db->prepare($query);
    $sth->bindParam(1, $args["origin"]);
    $sth->bindParam(2, $args["destination"]);
    $sth->execute();
    $flights = $sth->fetchAll();
    if ($sth->rowCount() > 0)
        return $this->response->withJson($flights);
    else
        return false;
    
});

// save flights to trip
$app->post('/saveflight', function (Request $request, Response $response, array $args) {
  $params = $request->getParams();
  $id_flight = $params["id"];
  $query = "INSERT INTO trips (id_flight) VALUES (?)";
  $sth = $this->db->prepare($query);
  $sth->bindParam(1, $id_flight);
  $sth->execute();
  return $this->response->withJson(['key' => "Saved trip - id : ".$this->db->lastInsertId()]);
});

// save flights to trip
$app->post('/removeFlightTrip', function (Request $request, Response $response, array $args) {
  $params = $request->getParams();
  $id_flight = $params["id"];
  $query = "DELETE FROM trips WHERE id_flight = ?";
  $sth = $this->db->prepare($query);
  $sth->bindParam(1, $id_flight);
  $sth->execute();
  return $this->response->withJson(['key' => "Removed flight - id : ".$id_flight]);
});


// get all flights
$app->get('/getAllFlights', function ($request, $response, $args) {
      $query = "SELECT fli.*, air.name as ori_name, air.city as ori_city, air2.name as dest_name, air2.city as dest_city,
                       line.name as airline_name
                FROM flights fli
                INNER JOIN airports air ON fli.originAirport = air.iata_code
                INNER JOIN airports air2 ON fli.destinationAirport = air2.iata_code
                INNER JOIN airlines line ON fli.airline = line.code";
    $sth = $this->db->prepare($query);
    $sth->bindParam(1, $args["origin"]);
    $sth->bindParam(2, $args["destination"]);
    $sth->execute();
    $flights = $sth->fetchAll();
    if ($sth->rowCount() > 0)
        return $this->response->withJson($flights);
    else
        return false;
    
});

// get all trips
$app->get('/getTrips', function ($request, $response, $args) {
      $query = "SELECT trips.*, fli.*, airlines.name AS airline_name from trips
                INNER JOIN flights fli ON trips.id_flight = fli.id
                INNER JOIN airlines ON fli.airline = airlines.code";
    $sth = $this->db->prepare($query);
    $sth->bindParam(1, $args["origin"]);
    $sth->bindParam(2, $args["destination"]);
    $sth->execute();
    $flights = $sth->fetchAll();
    if ($sth->rowCount() > 0)
        return $this->response->withJson($flights);
    else
        return false;
    
});


// JWT TOKEN
$app->post('/login', function (Request $request, Response $response, array $args) {
 
    $input = $request->getParsedBody();
    $sql = "SELECT * FROM users WHERE email= :email";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("email", $input['email']);
    $sth->execute();
    $user = $sth->fetchObject();
 
    // verify email address.
    if(!$user) {
        return $this->response->withJson(['error' => true, 'message' => 'These credentials do not match our records.']);  
    }
 
    // verify password.
    if (!$input['password'] == $user->password) {
        return $this->response->withJson(['error' => true, 'message' => 'These credentials do not match our records.']);  
    }
 
    $settings = $this->get('settings'); // get settings array.
    
    $token = JWT::encode(['id' => $user->id, 'email' => $user->email], $settings['jwt']['secret'], "HS256");
 
    return $this->response->withJson(['token' => $token]);
});




$app->group('/api', function(\Slim\App $app) {
 
    $app->get('/user',function(Request $request, Response $response, array $args) {
        print_r($request->getAttribute('decoded_token_data'));        
    });
  
   
});

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});



