<?php require_once("includes/header.php"); ?>
<!--body-->

     <!-- Loading panel  -->
     <div id="loading" class="text-center text-white"><i id="spinner" class="fas fa-plane fa-4x"></i></div>
     
     <!-- Title of page -->
     <div class="col-lg-8 offset-lg-2 pt-5">
       <h1 class="text-center text-white title mb-0">FLIGHT <span class="second-color">FINDER</span><h1>
         <h5 class="subtitle text-center text-white mt-4 mb-4">Stop Searching. <span class="second-color">Start Traveling.</span></h5>
     </div>
     
     <!-- Container of airport inputs -->
     <div id="inputContainer" class="col-lg-8 offset-lg-2 mt-2 text-center">
       <div class="row">
         
         <!-- ORIGIN AIRPORT -->
         <div class="col-md-4 offset-md-0 col-sm-10 offset-sm-1 col-xs-10 mb-2">
           <div class="do-splitflap">
            <h5 class="resized-splitflap">TAKING OFF</h5>
            <input id="origin" class="typeahead form-control form-control-lg" type="text">
           </div>
         </div>
         <div class="col-md-4 offset-md-0 col-sm-10 offset-sm-1 d-md-block d-none mb-4">
           <i id="compassRotate" class="far fa-compass fa-2x fa-rotate-90 mt-2 second-color"></i>
         </div>
         
         <!-- DESTINATION AIRPORT -->
         <div class="col-md-4 offset-md-0 col-sm-10 offset-sm-1 col-xs-10 mt-2 mt-sm-0">
           <div class="do-splitflap">
            <h5 class="resized-splitflap">LANDING</h5>
            <input id="destination" class="typeahead2 form-control form-control-lg" type="text">
           </div>
         </div>
       </div>
       
       <div class="row">
        <div class="col-lg-4 offset-lg-4">
          <a href="#" class="btn btn-rounded bg-second-color mt-4 mt-sm-0" onclick="searchFlights()">SEARCH <i class="fas fa-search"></i></a>
         </div>
       </div>
     </div>
         
     <div id="flights" class="d-none fadeIn text-center">
       
      <h5 id="fromTo" class="card-body text-center text-white mb-4 col-lg-8 offset-lg-2 ">
       <span class="btn btn-sm btn-rounded m-0 backButton" onclick="backToSearch()"><i class="fas fa-arrow-left"></i> BACK</span>
       FROM <span class="origin second-color"></span>
       TO <span class="destination second-color"></span></h5>
       
       <div id="flightsList"></div>
     </div>
         
      <div id="trips" class="d-none fadeIn text-center"> 
        <h5 id="yourTrips" class="card-body text-center text-white mb-4 col-lg-8 offset-lg-2 ">
         <span class="btn btn-sm btn-rounded m-0 backButton" onclick="backToFlights()"><i class="fas fa-arrow-left"></i> BACK</span>
         YOUR <span class="destination second-color">TRIP</span></h5>
         <div id="tripList"></div>
     </div>
         
         
          
<!-- /body -->   
<?php require_once("includes/footer.php"); ?>
         
<script id="oneFlight" type="text/x-custom-template">
  <div class="card flight mb-4 animated fadeIn col-lg-8 offset-lg-2 noOpacity">
    <div class="card-body">
      <div class="row">
       
       <div class="col-lg-4">
         <h4 class="card-title text-center">
            <span class="origin_code"></span> <i class="fas fa-arrow-right second-color"></i> <span class="dest_code"></span>
         </h4>
       </div>
       
       <div class="col-lg-4"><h4 class="flight_code text-center"></h4></div>
       <div class="col-lg-4"><h4 class="airline_name text-center"></h4></div>
         
      </div>
      <div class="row">
        <div class="col-lg-3"><small>date</small><h4 class="date text-center mb-0"></h4></div>
        <div class="col-lg-3"><small>time</small><h4 class="time text-center mb-0"></h4></div>
        <div class="col-lg-3"><small>duration</small><h4 class="duration text-center mb-0"></h4></div>
        <div class="col-lg-3"><small>&nbsp;</small><h4 class="text-center mb-0"><span class="price second-color"></span>$</h4></div>
      </div>
    </div>
  </div>
  </script>
         
<script id="tripsButton" type="text/x-custom-template">
  <div class="card mb-4 animated fadeIn col-lg-8 offset-lg-2 noOpacity p-0" onclick="showTrips()">
    <div class="card-body bg-second-color">
       SHOW ME MY TRIPS
    </div>
  </div>
</script>  