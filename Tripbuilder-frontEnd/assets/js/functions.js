
var height = $( window ).height();
var width = $( window ).width();
var originIata = "";
var destinationIata = "";
var flights_available = false;
var tripVisible = false;
var trip_available = false;


$(document).ready(function () {

/****** Transition after 800ms ******/  
  
  
 //$( "#spinner" ).animate({ top: "-="+height}, 1000);
  $("#spinner").transition({translate:[0, -height], rotate:'-90deg'},1000, 'in', function(){
    $('#loading').transition({translate:[0, -height]}, 1000, function(){
        $(this).remove();
     });
  });
  
  var ratio = 0.5;
  $('.resized-splitflap')
          .splitFlap({
              image:          'assets/img/chars.png',
              speed:          10,
	            speedVariation: 3,
              charWidth:  50 * ratio,
              charHeight: 100 * ratio,
              imageSize:  (2500 * ratio) + 'px ' + (100 * ratio) + 'px'
          });
  
  /********** COMPASS EFFECT **********/
  $("body").mousemove(function(event) {
    var compass = $("#compassRotate");
    var x = (compass.offset().left) + (compass.width() / 2);
    var y = (compass.offset().top) + (compass.height() / 2);
    var rad = Math.atan2(event.pageX - x, event.pageY - y);
    var rot = (rad * (180 / Math.PI) * -1)-40;
    compass.css({
      '-webkit-transform': 'rotate(' + rot + 'deg)',
      '-moz-transform': 'rotate(' + rot + 'deg)',
      '-ms-transform': 'rotate(' + rot + 'deg)',
      'transform': 'rotate(' + rot + 'deg)'
    });
  });
  
   /********* TYPE AHEAD FOR TAKE OFF AIRPORT ******/
   $('.typeahead').typeahead({
        minLength: 2,
        limit:10,
        source: function (query, process) {
            var origin = $(".typeahead").val().replace(/\s/g,"%20");
            //var link = 'https://loicbachellerie.com/api/airport/read_ahead.php?origin=';
            var link = "https://flights.loicbachellerie.com/tripbuilder/public/airports_ahead/";
            return $.get(link+origin, function (data) {
              return process(data);
            });
        },
        updater : function(item) {
            this.$element[0].value = item;
            originIata = getIata(item);
            return item;
        }
    }); 
  
   /****** TYPE AHEAD FOR LANDING AIRPORT ******/
  $('.typeahead2').typeahead({
        minLength: 2,
        limit:10,
        source: function (query, process) {
            var destination = $(".typeahead2").val().replace(/\s/g,"%20");
            var link = "https://flights.loicbachellerie.com/tripbuilder/public/airports_ahead/";
            return $.get(link+destination, function (data) {
              return process(data);
            });
        },
        updater : function(item) {
            this.$element[0].value = item;
            destinationIata = getIata(item);
            return item;
        }
    }); 
  
 
  /********* FLIGHTS **********/
  $(document).on('mouseenter','#flightsList .card.flight', function(){
    $(this).append('<i id="appendArrow" class="fas fa-plus-circle fa-2x second-color fadeIn"></i>');
    
  }).on('mouseleave','#flightsList .card.flight', function(){
    $("#appendArrow").remove();
  });
  
  $(document).on('click', "#flightsList .card.flight", function(){
    var id_flight = $(this).attr('id');
    saveFlightsSelected(id_flight);
  })
  /********** END **********/
  
  
  /********** TRIPS **********/
  $(document).on('mouseenter','#tripList .card.flightTrip', function(){
    $(this).append('<i id="removeArrow" class="fas fa-minus-circle fa-2x second-color fadeIn"></i>');
    
  }).on('mouseleave','#tripList .card.flightTrip', function(){
    $("#removeArrow").remove();
  });
  
  $(document).on('click', "#tripList .card.flightTrip", function(){
    var id_flight = $(this).attr('id');
    removeFlightTrip(id_flight);
  })
  /********** END **********/
  


  
}); // END document ready

function searchFlights(){
  
  if(originIata.length <=1 || destinationIata.length <=1)
    toastr.error("Please, make sure to enter 2 airports");
  else{
     $.ajax({
          url:"https://flights.loicbachellerie.com/tripbuilder/public/flights/"+originIata+"/"+destinationIata,
          method:"GET",
          dataType:"text",
      success: function(data){
        if(data != false){
          displayFlights(data);
        }
        else
          toastr.warning("We are sorry, no flights were found");
      },
      error: function(status){ console.log(status)},
  });
  
  }
  
}

function displayFlights(data){
  var flights = $.parseJSON(data);
  
  $("#inputContainer").transition({scale: 0.8}, 600, 'in', function() {
    $('#inputContainer').transition({translate:[-width*2, 0]}, 600, function(){
        if(flights_available){
          $("#flights").transition({translate:[0, 0]}, 600, 'in', function() {
            $("#flights").transition({scale: 1}, 600);
          });
        }
        $(this).addClass("d-none");
        flights_available = true;
        $("#flightsList").html("");
        $.each(flights, function(i, item){
          $("#flightsList").append($("#oneFlight").html());
          var last = "#flightsList .card:last-child";
          $(last).attr("id", item.id);
          $("#fromTo .origin").text(item.ori_city);
          $("#fromTo .destination").text(item.dest_city);
          $(last+' .origin_code').text(item.originAirport);
          $(last+' .dest_code').text(item.destinationAirport);
          $(last+' .flight_code').text(item.flight_code);
          $(last+' .airline_name').text(item.airline_name);
          $(last+' .duration').text(item.duration);
          $(last+' .date').text((item.dateTime).split(' ')[0]);
          $(last+' .time').text((item.dateTime).split(' ')[1]);
          $(last+' .price').text(item.price);
        })

       $("#flights").removeClass('d-none',function(){
         $("#flightsList card").each(function(index) {
          $(this).delay(600*index).fadeIn(300);
         });
       })

    });
  });
  }

function saveFlightsSelected(id){
  $.ajax({
      type: "POST",
      url: "https://flights.loicbachellerie.com/tripbuilder/public/saveflight",
      data: {"id":id},
      dataType:'json',
      success: function(data){
        console.log(data);
        if((data.key).indexOf("Saved trip") != -1){
          if(!tripVisible){
            tripVisible = true;
            $("#flightsList").prepend($("#tripsButton").html());
          }
          getMyTrips();
          toastr.success(data.key);
        }
         
      }
   });
}

function removeFlightTrip(id){
    $("#tripList .card#"+id).fadeOut();
    $.ajax({
      type: "POST",
      url: "https://flights.loicbachellerie.com/tripbuilder/public/removeFlightTrip",
      data: {"id":id},
      dataType:'json',
      success: function(data){
        console.log(data);
      }
   });
}

function getMyTrips(){
  $.ajax({
      type: "POST",
      url: "https://flights.loicbachellerie.com/tripbuilder/public/getTrips",
      method:"GET",
      dataType:"text",
      success: function(data){
        console.log("getMytrips");
        console.log(data);
        var flights = $.parseJSON(data);
        $("#tripList").html("");
        $.each(flights, function(i, item){
          $("#tripList").append($("#oneFlight").html());
          var last = "#tripList .card:last-child";
          $(last).attr("id", item.id).addClass("flightTrip");
          $("#fromTo .origin").text(item.ori_city);
          $("#fromTo .destination").text(item.dest_city);
          $(last+' .origin_code').text(item.originAirport);
          $(last+' .dest_code').text(item.destinationAirport);
          $(last+' .flight_code').text(item.flight_code);
          $(last+' .airline_name').text(item.airline_name);
          $(last+' .duration').text(item.duration);
          $(last+' .date').text((item.dateTime).split(' ')[0]);
          $(last+' .time').text((item.dateTime).split(' ')[1]);
          $(last+' .price').text(item.price);
        })
        
      }
   });
}

function showTrips(){
  $("#flights").transition({translate:[-width*2, 0]}, 600, function(){
     if(trip_available){
          $("#trips").transition({translate:[0, 0]}, 600, 'in', function() {
            $("#trips").transition({scale: 1}, 600);
          });
        }
     $(this).addClass("d-none");
     trip_available = true
     $("#trips").removeClass('d-none',function(){
       $("#tripList card").each(function(index) {
        $(this).delay(800*index).fadeIn(300);
       });
     })
  });
}

function backToSearch(){
  tripVisible = false;
  $("#flights").transition({scale: 0.8}, 600, 'in', function() {
     $("#flights").transition({translate:[width*2, 0]}, 600, function(){
        $('#inputContainer').removeClass("d-none").transition({translate:[0, 0]}, 600, function(){
          $('#inputContainer').transition({scale: 1}, 600);
        });
     });
 });
}

function backToFlights(){
    $("#trips").transition({scale: 0.8}, 600, 'in', function() {
     $("#trips").transition({translate:[width*2, 0]}, 600, function(){
        $('#flights').removeClass("d-none").transition({translate:[0, 0]}, 600, function(){
          $('#flights').transition({scale: 1}, 600);
        });
     });
 });
}

function getIata(item){
  var iata = item.substr(0, item.indexOf(' -'));
  return iata;
}

