# Trip Builder API

> ##Demo at [loicbachellerie.com](https://loicbachellerie.com)
> ### For the demo, try with the following airports.
> * Montreal YUL to [ Toronto YYZ , New York JFK , Vancouver YYZ , Mexico DF MEX , Paris CDG ]
> * Toronto YYW to Montreal YUL
> * Vancouver YVR to Montreal YUL
> * Ottawa YOW to New York JFK
> * Mexico MEX to Montreal YUL


* ** IF you have any problem with accents, make sure to set your charset to UTF-8**
* ** For PDO, add the following line to your new PDO object : **
> * array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::MYSQL_ATTR_FOUND_ROWS => true));
                  

## AIRPORTS

##### GET ALL AIRPORT NAMES
* No parameter
* http://flights.loicbachellerie.com/tripbuilder/public/airport_names
* Response will be in JSON Format
* Response example :  **{"name":"Montreal \/ Pierre Elliott Trudeau International Airport"}**

##### GET ALL AIRPORTS CONTAINING (PARAMETER)
* The parameter can be **name**, the **IATA_CODE** or the **city** 
* https://flights.loicbachellerie.com/tripbuilder/public/airports_ahead/**parameter**
* Example : parameter = **JFK**

> * RESPONSE :  **["JFK - John F Kennedy International Airport - New York - US"]**

* The response contains basic information, if you want more information, have a look a the next section

##### GET ALL AIRPORT DATA
* http://flights.loicbachellerie.com/tripbuilder/public/airports
* Response will be in JSON Format
* Response example for one airport

> * {"id":"39331",
> * "code":"CYUL",
> * "type":"large_airport",
> * "name":"Montreal \/ Pierre Elliott Trudeau International Airport",
> * "elevation":"118",
> * "continent":"NA",
> * "country":"CA",
> * "region":"CA-QC",
> * "city":"Montr\u00e9al",
> * "gps_code":"CYUL",
> * "iata_code":"YUL",
> * "local_code":"YUL",
> * "coor1":"-73.7407989502",
> * "coor2":"45.4706001282"}**



## FLIGHTS

##### FIND FLIGHTS FROM ORIGIN TO DESTINATION
* The parameters can only be from an **IATA CODE** to another **IATA CODE***
* https://flights.loicbachellerie.com/tripbuilder/public/flights/**originIata**/**destinationIata**
* Example : http://flights.loicbachellerie.com/tripbuilder/public/flights/**YUL**/**JFK**

> * {"id":"4",
> * "flight_code":"UA320",
> * "airline":"UA",
> * "originAirport":"YUL",
> * "destinationAirport":"JFK",
> * "dateTime":"2018-08-27 10:10:00",
> * "duration":"01:35:00",
> * "planeId":"2",
> * "status":"1",
> * "price":"255.25",
> * "ori_name":"Montreal \/ Pierre Elliott Trudeau International Airport",
> * "ori_city":"Montr\u00e9al",
> * "dest_name":"John F Kennedy International Airport",
> * "dest_city":"New York",
> * "airline_name":"United Air Lines Inc."}

###### GET ALL FLIGHTS
* No parameters
* http://flights.loicbachellerie.com/tripbuilder/public/getAllFlights
* Response will be in JSON Format
* Response example : This response contains the same keys as FIND FLIGHTS FROM ORIGIN TO DESTINATION

###### SAVE FLIGHTS TO A TRIP
* The parameter is the **id** of the flight
* http://flights.loicbachellerie.com/tripbuilder/public/saveflight/
* The api expects to to find a JSON with an "id" key containing your flight id
* If correctly saved, 2 being the last inserted trip, the API sends back **"Saved trip - id : 2"**
* If using AJAX, make sure to include these parameters

   > * type : 'POST',
   > * url : 'https://flights.loicbachellerie.com/tripbuilder/public/**saveflight**',
   > * data: {'id' : id_of_your_flight},
   > * dataType : 'json'
   
###### REMOVE FLIGHTS FROM A TRIP
* The parameter is the **id** of the flight
* http://flights.loicbachellerie.com/tripbuilder/public/removeFlightTrip/
* The api expects to to find a JSON with an "id" key containing your flight id
* If correctly remove, 2 being the flight id, the API sends back **"Removed flight - id : 2"**
* If using AJAX, make sure to include these parameters

   > * type : 'POST',
   > * url : 'https://flights.loicbachellerie.com/tripbuilder/public/**removeFlightTrip**',
   > * data: {'id' : id_of_your_flight},
   > * dataType : 'json'
   
##### GET TRIPS
* No parameters
* Example : http://flights.loicbachellerie.com/tripbuilder/public/getTrips/
* Response : 

> * {"id":"4",
> * "id_flight":"3",
> * "flight_code":"UA320",
> * "airline":"UA",
> * "originAirport":"YUL",
> * "destinationAirport":"JFK",
> * "dateTime":"2018-08-27 10:10:00",
> * "duration":"01:35:00",
> * "planeId":"2",
> * "status":"1",
> * "price":"255.25",
> * "ori_name":"Montreal \/ Pierre Elliott Trudeau International Airport",
> * "ori_city":"Montr\u00e9al",
> * "dest_name":"John F Kennedy International Airport",
> * "dest_city":"New York",
> * "airline_name":"United Air Lines Inc."}


##### LOGIN JSON WEB TOKEN
* ** Working, but is still under development**
* Parameters are **email** and **password**
* http://flights.loicbachellerie.com/tripbuilder/public/**login**
* POST with the following data {"email":"test@test.com", "password":"test"}
* Response : 
> * {
    "token": "eyJ0eXAiOiJKV1QiLCJhbOR1RiJ9.eyJpZCI6IjEW..."
}

